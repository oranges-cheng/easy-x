#define  _CRT_SECURE_NO_WARNINGS 1
//包含图形库头文件
#include <graphics.h>        //前面设置的颜色会影响后面，不想被影响就需要重新设置

#include <stdio.h>
#include <easyx.h>
#include <conio.h>

int main()
{
	//initgraph(800, 600, SHOUCONSOLE);
	// int flag
	//SHOUCONSOLE  创建图形窗口时，保留控制台的显示
	//NOCLOSE   没有关闭功能
	//NOMINIMIZE  没有最小化功能
	initgraph(800, 600); //物理坐标，创建绘图窗口， 单位像素
	setorigin(400, 300); //设置逻辑原点坐标
	//setaspectratio(1, -1); // x, y 为负数时翻转坐标轴  //翻转坐标轴后，绘制的文字也会翻转

	//设置背景颜色
	//setbkcolor(WHITE); 
	setbkcolor(RGB(50, 158, 151));
	//清屏 清除掉系统默认的背景黑色，展现设置的背景颜色
	cleardevice();


	//设置线条， 填充颜色等
	setlinestyle(PS_SOLID, 5); //设置线条样式   ？？
	setfillcolor(CYAN); //设置填充的颜色   
	setlinecolor(YELLOW); //设置线条颜色 


	//输出绘制文字
	//设置文字颜色
	settextcolor(RED);
	//自己设置颜色
	//settextcolor( RGB(三原色的值)）    ？？？

	//设置文字样式： 大小， 字体.....  ？？？
	settextstyle(50, 0, L"楷体");

	//设置文字背景模式 文字默认自带背景，可能会遮盖住原有的图像
	setbkmode(TRANSPARENT); // transparent 透明的

	//outtextxy(0, 0, 'a');        //??  字符串 字体默认时白色 单个字符不需要下面操作

	//outtextxy(0, 0, "hello");  //字符集导致的错误 参数错误找不到对应的函数
	//解决方法一： 在字符串前面加上大写字母L
	//outtextxy(0, 0, L"hello");

	//解决方法二：  TEXT(字符串）   _T()  最终都是L
	//outtextxy(0, 0, TEXT("hello"));
	//outtextxy(0, 0, _T("hello"));

	//解决方法三：配置，属性， 常规， 字符集， 改为多字节字符集
	//outtextxy(0, 0, "hello");

	//把文字居中在自己设置的文本框中  ?????
	fillrectangle(-200, 150, 200, -150); //画出文本框
	//算出需要表现的字符串的宽度 计算在文本框中居中位置的距离，最后和图形界面的距离
	//char arr[] = "hello world!";
	//int width = 200 - textwidth(arr) / 2;
	//int height = 150 - textheight(arr) / 2;
	//outtext(width + 200, height + 150, arr);
	
	
	//画圆
	circle(0, 0, 300); //无填充
	//fillcircle(0, 0, 300);//有边框填充
	//solidcircle(0, 0, 300); //无边框填充

	//int flag 设置时 可以保留控制台的显示
	/*int i = 0;
	scanf("%d", &i);
	printf("%d\n", i);*/

	//输出图片
	//定义一个对象（变量）
	IMAGE img;
	//加载图像
	// ./ 表示当前文件夹，  ../ 表示当前文件夹上一级
	//相对路径： ./  ../
	//绝对路径："D:\日常代码合集\graphcis1_1\graphcis1_1\shenhe.JPG"
	loadimage(&img, L"./shenhe.jpg", 300, 400);
	//输出图像
	putimage(100, 150, &img); //左上角的位置


	//鼠标消息函数
	//循环接受鼠标消息
	while (1)
	{
		//判断是否有鼠标信息
		if (MouseHit())
		{
			//有鼠标点击， 创建对象 获取鼠标消息， 赋给 msg变量
			MOUSEMSG msg = GetMouseMsg();

			//消息分发
			switch (msg.uMsg)
			{
				//当左键按下
			case WM_LBUTTONDOWN:
				outtextxy(-200, -150, L"申鹤");
				printf("xy(%d, %d)\n", msg.x, msg.y); //在控制台显示鼠标点击的位置

				//判断鼠标点击的位置，是否在自己预期的位置
				if (msg.x >= -200 && msg.x <= 200 && msg.y >= -150, msg.y <= 150)
				{
					printf("芜湖, 起飞\n");
				}
				break;
			case WM_RBUTTONDOWN:
				outtextxy(-200, -150, L"红绳");
				printf("xy(%d, %d)\n", msg.x, msg.y); //在控制台显示鼠标点击的位置
				break;
			}

		}
	}
	getchar();    //按任意键继续，防止运行后立刻关闭
	closegraph();  //关闭绘图窗口
	return 0;
}