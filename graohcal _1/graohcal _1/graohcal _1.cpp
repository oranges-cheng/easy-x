﻿#include <graphics.h>		// 引用图形库头文件
#include <conio.h>
#include <easyx.h>
#include <stdio.h>
#include <math.h>
#define PI 3.14
int main()
{
	initgraph(800, 600);	// 创建绘图窗口，大小为 640x480 像素
	setorigin(400, 300);    //设置逻辑坐标原点
	setaspectratio(1, -1);  //( x, y) 为负数将翻转坐标轴
	
	//多边形的顶点， 数组
	//POINT points[] = { {0, 200}, {200, -200}, {-200, -200} }; //顺序连接
	//绘制多边形的数组， 数组名（首地址） 顶点的个数
	//polygon(points, 3);

	//POINT points[] = { {-100, 100}, {100, 100}, {200, -100}, {-200, -100} };
	//polygon(points, 4);

	/*double theta = PI / 2;
	double delta = 2 * PI / 5;
	int r = 200;
	POINT points[5];
	for (int i = 0; i < 5; i++)
	{
		points[i].x = cos(theta + i * delta) * r;
		points[i].y = sin(theta + i * delta) * r;
	}*/

	POINT points[5];
	double theta = PI / 2;
	double delta = PI * 2 / 5;
	int r = 200;
	for (int i = 0; i < 5; i++)
	{
		points[i].x = cos(theta + i * delta) * r;
		points[i].y = sin(theta + i * delta) * r;
	}
	setlinecolor(CYAN);
	//polygon(points, 5);  //一次连接各个点，最后会成为封闭的图形
	polyline(points, 5);//和polygon函数差不多， 不过他不会连接首尾的点，因此不会封闭；

	int x, y;
	for (int i = 0; i < 100; i++)
	{
		x = rand() % (800 + 1) - 400;
		y = rand() % (600 + 1) - 300;
		putpixel(x, y, CYAN);
	}

	getchar();				// 按任意键继续
	closegraph();			// 关闭绘图窗口
	return 0;
}


