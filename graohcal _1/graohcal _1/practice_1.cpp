//#define  _CRT_SECURE_NO_WARNINGS 1
//#include <graphics.h>
//#include <conio.h>
//#include <stdio.h>
//#define pi 3.14
//
//int main()
//{
//	initgraph(800, 600); //物理坐标  创建绘图窗口，像素
//	setorigin(400, 300);  //设置逻辑坐标
//	setaspectratio(1, -1);  // x y 为负数翻转坐标轴
//
//	//绘制点
//	putpixel(200, 200, RED);
//
//	//绘制随机点
//	int x, y;
//	for (int i = 0; i < 100; i++)
//	{
//		x = rand() % (800 + 1) - 400;
//		y = rand() % (600 + 1) - 300;
//		putpixel(x, y, CYAN);
//	}
//
//	//绘制线
//	line(-200, 200, 200, -200);
//
//	//绘制圆
//	circle(0, 0, 200);
//
//	//绘制矩形
//	rectangle(-200, 100, 200, -100);
//
//	//绘制椭圆
//	ellipse(-200, 100, 200, -200);
//
//	//绘制圆角矩形
//	roundrect(-200, 100, 200, -100, 200, 100); //矩形加 四个椭圆的 长和高
//
//	//绘制扇形
//	pie(-200, 100, 200, -100, 0, pi / 4);
//
//	//绘制圆弧
//	arc(-200, 100, 200, -100, 0, pi / 4);
//
//
//	getchar();   //按任意键继续，防止运行后立刻关闭
//	closegraph(); //关闭绘图窗口
//	return 0;
//}